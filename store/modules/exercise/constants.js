export default {
    DO_CREATE: 'exercise/doCreate',
    DO_LIST: 'exercise/doList',
    DO_DELETE: 'exercise/doDelete',
}
