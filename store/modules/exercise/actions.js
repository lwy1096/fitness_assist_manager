import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_CREATE]: (store, payload) => {
        console.log(payload)
        return axios.post(apiUrls.DO_CREATE.replace('{id}', payload.id), payload.data)
    },
    [Constants.DO_LIST]: (store) => {
        return axios.get(apiUrls.DO_LIST)
    },
    [Constants.DO_DELETE]: (store, payload) => {
        return axios.delete(apiUrls.DO_DELETE.replace('{id}', payload.id))
    },
}
