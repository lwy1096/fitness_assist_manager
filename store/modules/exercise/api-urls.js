const BASE_URL = '/v1/exercise'

export default {
    DO_CREATE: `${BASE_URL}/member/exercise/{id}`, //post
    DO_LIST: `${BASE_URL}/member/all`, //get
    DO_DELETE: `${BASE_URL}/{id}`, //del
}
